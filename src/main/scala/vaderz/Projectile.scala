package vaderz

class Projectile(game: Game, projectile_pos: Int) {
  var ypos = 500
  var hit_alien = false
  val projectile_width = 4
  val projectile_length = 15
  var filter_break = true

  def draw() = {
    ypos -= 9
    game.rect(projectile_pos, ypos, projectile_width, projectile_length)
  }

  def offScreenFilter() = {
    if (ypos + projectile_length < 0) { filter_break = false }
    filter_break
  }

  def hitAlienFilter() = {
    if (hit_alien == true) { filter_break = false }
    filter_break
  }

  def position() = {
    projectile_pos
  }
}