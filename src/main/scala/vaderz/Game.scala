package vaderz
import processing.core._
import processing.core.PConstants._
import scala.util.Random
import java.util.Arrays.ArrayList
import java.io.File
trait Alien {
  def draw()
}

class Game extends MyPApplet {
  var backgroundImg: PImage = null
  backgroundImg = this.loadImage("../resources/background.png")
  val cannon = new Cannon(this)
  val alienfactory = new AlienFactory(this)
  var projectiles: List[Projectile] = List()
  val screenWidth = 800
  val screenLength = 600
  var masterRow: List[List[Alien]] = List()
  val alienInRow = 12
  var backgroundYpos = -600
  var keyleft = false
  var keyright = false
  var keyfire = false

  override def setup() = {
    size(screenWidth, screenLength) // Size must be the first statement
    frameRate(60)
    stroke(0) // Set line drawing color to white
    masterRow = masterRow :+ alienfactory.createRowOne()
    masterRow = masterRow :+ alienfactory.createRowTwo()

  }
  override def draw() = {
    this.image(backgroundImg, 0, backgroundYpos)
    moveBackground()
    cannon.draw()
    masterRow.foreach { _.foreach { _.draw() } }
    projectiles.map(_.draw())
    projectiles = projectiles.filter(_.offScreenFilter)
    projectiles = projectiles.filter(_.hitAlienFilter)
    cannon.reload()

    if (keyleft) {
      cannon.left()
    }
    if (keyright) {
      cannon.right()
    }
    if (keyfire) {
      cannon.fire
    }
  }

  override def keyPressed() = {
    if (keyCode == LEFT) {
      keyleft = true
    } else if (keyCode == RIGHT) {
      keyright = true
    }
    if (key == ' ') {
      keyfire = true
    }
  }

  override def keyReleased() = {
    if (keyCode == LEFT) {
      keyleft = false
    }
    if (keyCode == RIGHT) {
      keyright = false
    }
    if (key == ' ') {
      keyfire = false
    }
  }
  def moveBackground() = {
    backgroundYpos += 10
    if (backgroundYpos == 0) {
      backgroundYpos = -600
    }
  }
}

