package vaderz
import processing.core._
class AlienOne(game: Game, ordinal: Int) extends Alien {
  //var alienHealth = 2
  var ypos = -38
  val delta_x = 38
  val delta_y = 38
  var am_i_hit = false
  var filter_break = true
  var alienImg: PImage = game.loadImage("../resources/alien.jpeg")
  var moverCounter = 1

  def figureOutMyXPos() = {
    val screenSplitter = (game.screenWidth / game.alienInRow)
    val middleOfSpace = screenSplitter / 2
    val spotChooser = screenSplitter * ordinal
    val findMiddleOfNeededSpace = spotChooser - middleOfSpace
    val alienXpos = findMiddleOfNeededSpace - (delta_x / 2)
    alienXpos
  }

  def give_hit() = {
    am_i_hit
  }
  def draw(): Unit = {
    if (am_i_hit) return
    checkHitBox()
  }

  def checkHitBox() = {
    val xpos = figureOutMyXPos()
    shouldIMove()
    alienWins()
    game.image(alienImg, xpos, ypos)
    for (projectile <- game.projectiles) {
      //hit box
      //left hand side is alien data, right hand is box data
      if (ypos + delta_y >= projectile.ypos && //this calculates the top of the projectile and the bottom of the alien
        ypos < projectile.ypos + projectile.projectile_length &&
        xpos + delta_x > projectile.position && //right X boundary
        xpos < projectile.position() + projectile.projectile_width) //left X boundary, fixed 1- pixel hitbox bug 
        {
        projectile.hit_alien = true
        //  alienHealth -= 1
        //  if (alienHealth == 0){
        am_i_hit = true
      }
    }

  }
  def shouldIMove() = {
    if (moverCounter == 4) {
      ypos += 1
      moverCounter = 1
    } else if (moverCounter < 4) {
      moverCounter += 1
    }
  }
  def alienWins() = {
    if (ypos + delta_y == game.cannon.ypos)
      println("Game over")
  }
}