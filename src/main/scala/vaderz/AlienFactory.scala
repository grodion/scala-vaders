package vaderz

class AlienFactory(game: Game) {
  def createRowOne() = {
    List.range(1, game.alienInRow + 1).map(new AlienOne(game, _))
    //needed to add one because range didn't generate 12
  }
  def createRowTwo() = {
    List.range(1, game.alienInRow + 1).map(new AlienTwo(game, _))
  }
}