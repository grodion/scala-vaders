package vaderz
import processing.core._
class Cannon(game: Game) {
  val delta = 10
  var xpos = 1
  val ypos = 500
  var width = 85
  var cannon_reload = 30 //Limit on how fast cannon reloads
  val cannonImg: PImage = game.loadImage("../resources/cannon.jpeg")

  def draw() = {
    game.image(cannonImg, xpos, ypos)
  }

  def left() = {
    if (xpos > 0) {
      xpos -= delta
    }
  }

  def right() = {
    if (xpos < game.width - this.width) {
      xpos += delta
    }

  }
  def fire() = {
    if (cannon_reload == 30) {
      var projectile_pos = xpos + width / 2 - 2
      val projectile = new Projectile(game, projectile_pos)
      //cannot pass half of projectile width because projectile is created in same spot
      game.projectiles = game.projectiles :+ projectile
      cannon_reload = 0
    }
  }
  def reload() = {
    if (cannon_reload < 30) {
      cannon_reload += 1
    }
  }
}
